import React from 'react';

const TableDisplay = (props) => {
    return (
        <>
        <table>
            <tbody style = {{ display: 'flex' } } >
                 {props.firstRow.length > 0 && props.firstRow.split(",").map((numList, i) => ( 
                   <tr key = { i } > {
                        numList.split(",").map((num, j) =>
                            
                        <td key = { j } > { num } </td>
                        )
                    } 
                    </tr>
                ))
            } 
            </tbody> 
            </table>      
            <table >
            <thead >
            </thead> 
            <tbody style = {{ display: 'flex' }} > 
            { props.secondRow.length > 0 && props.secondRow.split(",").map((numList, i) => ( 
                    <tr key = { i } > {
                        numList.split(",").map((num, j) =>
                           
                        <td key = { j } > { num } </td>
                        )
                    } 
                    </tr>
                ))
            } 
            </tbody> 
            </table> 
            </>  
      );
}
 
export default TableDisplay;