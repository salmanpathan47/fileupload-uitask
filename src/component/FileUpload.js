import React, { Component } from 'react';
import axios from 'axios';
import './FileUpload.css'

class FileUpload extends Component {
    state = {
        selectedFile: null,
        firstRow: "",
        secondRow: ""
    }

    handlefile = (result) => {
        let fileContent;
        if (result.currentTarget.result.indexOf(',') !== -1) {
            fileContent = result.currentTarget.result.split("\n")
        } else {
            fileContent = result.currentTarget.result.split("\n")
        }
        this.setState({
            firstRow: fileContent[0],
            secondRow: fileContent[1]
        })
    }

    onChangeHandler = event => {
        var reader = new FileReader();
        reader.onloadend = this.handlefile
        reader.readAsText(event.target.files[0]);
        this.setState({
            selectedFile: event.target.files[0],
            loaded: 0,
        })
    }

    onClickHandler = () => {
        const data = new FormData()
        data.append('file', this.state.selectedFile)
        axios.post("http://localhost:8000/upload", data, {})
            .then(res => {
                console.log("File Uploaded", res.statusText)
            })
    }

    render() {

        return ( 
        <div className = "container" >
            <div className = "row" style={{justifyContent: 'center'}} >
            <div className = "col-md-8" >
            <form method = "post"
            action = "#"
            id = "#" 
            >
            <div className = "form-group files" style={{textAlign:'center'}} >
            <label> Upload Your File </label> 
            < input type = "file"
            name = "file"
            onChange = {
                (e) => this.onChangeHandler(e) }
            />
             </div> 
            <table>
            <tbody style = {{ display: 'flex' } } >
                 {this.state.firstRow.length > 0 && this.state.firstRow.split(",").map((numList, i) => ( 
                   <tr key = { i } > {
                        numList.split(",").map((num, j) =>
                            
                        <td key = { j } > { num } </td>
                        )
                    } 
                    </tr>
                ))
            } 
            </tbody> 
            </table>      
            <table >
            <thead >
            </thead> 
            <tbody style = {{ display: 'flex' }} > 
            { this.state.secondRow.length > 0 && this.state.secondRow.split(",").map((numList, i) => ( 
                    <tr key = { i } > {
                        numList.split(",").map((num, j) =>
                           
                        <td key = { j } > { num } </td>
                        )
                    } 
                    </tr>
                ))
            } 
            </tbody> 
            </table>   
            <button type = "button" className = "btn btn-success btn-block"
            onClick = { this.onClickHandler } 
            style ={{    marginTop: '28px'}}
            > Upload </button> 
            </form>     
            </div>
            </div> 
            </div>
        );
    }
}

export default FileUpload;